function bukaHal(pageName, elmnt, color) {
    var i, tabcontent, tab;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    tab = document.getElementsByClassName("tab");
    for (i = 0; i < tab.length; i++) {
        tab[i].style.backgroundColor = "";
        tab[i].style.color = "";
    }

    document.getElementById(pageName).style.display = "block";
    elmnt.style.backgroundColor = color;
    elmnt.style.color = "white";
}
document.getElementById("buka").click();

const phi = 3.14;
var luas;
var keliling;
var r;
function sumLuas() {
    r = parseFloat(document.getElementById('jari').value);
    luas = phi*Math.pow(r,2);
    let hasil_luas = document.getElementById('luas');
    hasil_luas.innerHTML = "Luas lingkaran dengan jari-jari " + r + "cm adalah " + luas;
}
                        
function sumKel() {
    r = parseFloat(document.getElementById('jari').value);
    keliling = phi*2*r;
    let hasil_keliling = document.getElementById('keliling');
    hasil_keliling.innerHTML = "Keliling lingkaran dengan jari-jari " + r + "cm adalah " + keliling;
}

var input;
var hasil;
function conv() {
    input = parseFloat(document.getElementById("rp").value);
    hasil = input/14650;
    document.getElementById("hasil").value = "$ " + hasil;
}

function penjumlahan() {
    let a = parseFloat(document.getElementById('angka1').value);
    let b = parseFloat(document.getElementById('angka2').value);
    let hitung = a + b;
    document.getElementById('hitung').value = hitung;
}

function parkir() {
    let masuk = parseInt(document.getElementById('in').value);
    let keluar = parseInt(document.getElementById('out').value);
    let minus = keluar - masuk;
    let tarif = 0;
    const tarif_awal = 3000;
    const tarif_tambah = 1000;
    if (minus < 2) {
        tarif = tarif_awal;
    }
    else {
        let add_jam = minus - 2;
        tarif = tarif_awal + (tarif_tambah * add_jam);
    }
    let t_tarif = document.getElementById('tarif');
    t_tarif.innerHTML = "Mohon bayar senilai Rp. " + tarif;
}
